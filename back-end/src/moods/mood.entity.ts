import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { TravelMood } from '../travelMoods/travelMood.entity';

@Entity('moods')
export class Mood {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @OneToMany(() => TravelMood, (travelMood) => travelMood.mood)
  travels: TravelMood[];
}
