import { IsNotEmpty } from 'class-validator';

export class MoodDto {
  @IsNotEmpty()
  id: string;

  @IsNotEmpty()
  name: string;
}
