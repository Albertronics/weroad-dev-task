import { Test, TestingModule } from '@nestjs/testing';
import { MoodsService } from './moods.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Mood } from './mood.entity';

const moods = [
  {
    id: '94204aea-4713-4c6f-9307-49d66340fb05',
    name: 'history',
  },
  {
    id: '28e7bba4-d280-40b1-833f-ca64c9af1dd9',
    name: 'nature',
  },
  {
    id: '410ba91a-34b7-4528-849b-eacb833fb79a',
    name: 'relax',
  },
  {
    id: '19eeb360-fe8c-478a-9f3d-1bc267a2f6f2',
    name: 'culture',
  },
  {
    id: '835ae233-8993-46ac-8c6d-76817051736a',
    name: 'party',
  },
];

describe('MoodsService', () => {
  let service: MoodsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        MoodsService,
        {
          provide: getRepositoryToken(Mood),
          useValue: {
            find: jest.fn().mockResolvedValue(moods),
          },
        },
      ],
    }).compile();

    service = module.get<MoodsService>(MoodsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should return all moods', async () => {
    const moodsResult = await service.findAll();

    expect(moodsResult).toEqual(moods);
  });
});
