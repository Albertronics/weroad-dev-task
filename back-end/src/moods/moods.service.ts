import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Mood } from './mood.entity';

@Injectable()
export class MoodsService {
  constructor(
    @InjectRepository(Mood)
    private moodRepository: Repository<Mood>,
  ) {}

  findAll(): Promise<Mood[]> {
    return this.moodRepository.find();
  }
}
