import { Controller, Get, UseGuards } from '@nestjs/common';
import { RolesGuard } from '../roles/roles.guard';
import { RoleEnum, Roles } from '../roles/roles.decorator';
import { MoodsService } from './moods.service';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Moods')
@Controller('moods')
export class MoodsController {
  constructor(private moodsService: MoodsService) {}

  @Get()
  @UseGuards(RolesGuard)
  @Roles(RoleEnum.EDITOR, RoleEnum.ADMIN)
  async getAll() {
    return this.moodsService.findAll();
  }
}
