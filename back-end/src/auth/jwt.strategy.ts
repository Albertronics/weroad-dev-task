import { Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { Request } from 'express';
import { jwtConstants } from './constants';
import { JwtPayloadDto } from './jwtPayload.dto';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor() {
    super({
      jwtFromRequest: (req: Request) => {
        if (!req) return null;

        const { accessToken } = req.cookies;

        if (accessToken && !req.headers.authorization) {
          req.headers.authorization = `Bearer ${accessToken}`;
        }

        const authHeader: string = req.headers.authorization;

        return authHeader && authHeader.split(' ')[1];
      },
      ignoreExpiration: false,
      secretOrKey: jwtConstants.secret,
    });
  }

  async validate(payload: JwtPayloadDto) {
    return { userId: payload.id, email: payload.email, roles: payload.roles };
  }
}
