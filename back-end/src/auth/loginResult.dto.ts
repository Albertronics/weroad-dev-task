import { JwtPayloadDto } from './jwtPayload.dto';

export class LoginResultDto {
  accessToken: string;
  expiresAt: Date;
  userData: JwtPayloadDto;
}
