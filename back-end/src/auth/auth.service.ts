import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { CredentialDto } from './credentials.dto';
import { LoginResultDto } from './loginResult.dto';
import { User } from '../users/user.entity';
import { JwtPayloadDto } from './jwtPayload.dto';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  private async validateUser(email: string, pass: string): Promise<User> {
    const user: User = await this.usersService.findByEmail(email);

    if (!user) {
      return null;
    }

    const passwordMatches: boolean = await bcrypt.compare(pass, user.password);

    if (user && passwordMatches) {
      return user;
    }

    return null;
  }

  async login(credentials: CredentialDto): Promise<LoginResultDto> {
    const user: User = await this.validateUser(
      credentials.email,
      credentials.password,
    );

    if (!user) {
      throw new UnauthorizedException('Wrong email or password!');
    }

    const payload: JwtPayloadDto = {
      email: user.email,
      id: user.id,
      roles: user.roles.map((el) => el.name),
    };

    return {
      accessToken: this.jwtService.sign(payload),
      expiresAt: new Date(Date.now() + 60 * 1000 * 60 * 7),
      userData: payload,
    };
  }
}
