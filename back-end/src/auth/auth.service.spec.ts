import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from './constants';
import { User } from '../users/user.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { UsersService } from '../users/users.service';

const basicUser = new User(
  'f68ec16e-0d05-4dd0-899f-4c10448ec3e6',
  'albertomusciumarra@gmail.com',
  '$2b$12$RP6dMYdCorkHTvVVXCFMqOy53BfVpkRzyKqTsiGDErbRmmUjXu1Eq',
  [{ id: 'b64f0542-4203-4e6a-b57d-2f4f7d868e16', name: 'admin' }],
);

describe('AuthService', () => {
  let service: AuthService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        JwtModule.register({
          secret: jwtConstants.secret,
          signOptions: { expiresIn: '7h' },
        }),
      ],
      providers: [
        AuthService,
        UsersService,
        {
          provide: getRepositoryToken(User),
          useValue: {
            find: jest.fn().mockResolvedValue([basicUser]),
          },
        },
      ],
    }).compile();

    service = module.get<AuthService>(AuthService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('correctly logs in user', async () => {
    const loginResult = await service.login({
      email: 'albertomusciumarra@gmail.com',
      password: '1234',
    });

    expect(loginResult.accessToken).toBeDefined();
    expect(loginResult.userData).toStrictEqual({
      email: 'albertomusciumarra@gmail.com',
      id: 'f68ec16e-0d05-4dd0-899f-4c10448ec3e6',
      roles: ['admin'],
    });
  });

  it('blocks wrong password', async () => {
    try {
      await service.login({
        email: 'albertomusciumarra@gmail.com',
        password: '12345',
      });

      expect(true).toBe(false);
    } catch (e) {
      expect(e.message).toBe('Wrong email or password!');
    }
  });

  it('blocks wrong email', async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        JwtModule.register({
          secret: jwtConstants.secret,
          signOptions: { expiresIn: '7h' },
        }),
      ],
      providers: [
        AuthService,
        UsersService,
        {
          provide: getRepositoryToken(User),
          useValue: {
            find: jest.fn().mockResolvedValue([]),
          },
        },
      ],
    }).compile();

    service = module.get<AuthService>(AuthService);

    try {
      await service.login({
        email: 'test@gmail.com',
        password: '1234',
      });

      expect(true).toBe(false);
    } catch (e) {
      expect(e.message).toBe('Wrong email or password!');
    }
  });
});
