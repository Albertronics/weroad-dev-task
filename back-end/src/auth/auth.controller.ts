import {
  Controller,
  Request,
  Post,
  UseGuards,
  Get,
  Body,
  Res,
  HttpCode,
} from '@nestjs/common';
import { JwtAuthGuard } from './jwt-auth.guard';
import { AuthService } from './auth.service';
import { Response } from 'express';
import { CredentialDto } from './credentials.dto';
import { LoginResultDto } from './loginResult.dto';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { JwtPayloadDto } from './jwtPayload.dto';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @ApiOkResponse({
    type: LoginResultDto,
  })
  @HttpCode(200)
  @Post('login')
  async login(
    @Body() credentials: CredentialDto,
    @Res({ passthrough: true }) response: Response,
  ): Promise<LoginResultDto> {
    const loginResult: LoginResultDto = await this.authService.login(
      credentials,
    );

    response.cookie('accessToken', loginResult.accessToken, {
      httpOnly: true,
      expires: loginResult.expiresAt,
    });

    return loginResult;
  }

  @Post('logout')
  @HttpCode(204)
  async logout(@Res({ passthrough: true }) response: Response): Promise<void> {
    response.clearCookie('accessToken');
  }

  @UseGuards(JwtAuthGuard)
  @Get('profile')
  getProfile(@Request() req): JwtPayloadDto {
    return req.user;
  }
}
