import { SetMetadata } from '@nestjs/common';

export enum RoleEnum {
  EDITOR = 'editor',
  ADMIN = 'admin',
}

export const ROLES_KEY = 'roles';
export const Roles = (...roles: RoleEnum[]) => SetMetadata(ROLES_KEY, roles);
