import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  AfterLoad,
} from 'typeorm';
import { TravelMood } from '../travelMoods/travelMood.entity';
import { Transform } from 'class-transformer';
import { Tour } from '../tours/tour.entity';

@Entity('travels')
export class Travel {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ default: false })
  isPublic: boolean;

  @Column()
  slug: string;

  @Column({ unique: true })
  name: string;

  @Column({ type: 'text' })
  description: string;

  @Column({ type: 'smallint' })
  numberOfDays: number;

  @OneToMany(() => TravelMood, (travelMood) => travelMood.travel)
  @Transform(({ value }) => {
    return value.reduce(
      (obj, item) => ({
        ...obj,
        [item.mood.name]: item.value,
      }),
      {},
    );
  })
  moods: TravelMood[];

  @OneToMany(() => Tour, (tour) => tour.travel)
  tours: Tour[];

  numberOfNights: number;

  @AfterLoad()
  private afterLoad() {
    this.numberOfNights = this.numberOfDays - 1;
  }
}
