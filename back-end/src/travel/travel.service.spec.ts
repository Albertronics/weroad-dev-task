import { Test, TestingModule } from '@nestjs/testing';
import { TravelService } from './travel.service';
import { MoodsService } from '../moods/moods.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Tour } from '../tours/tour.entity';
import { Travel } from './travel.entity';
import { Mood } from '../moods/mood.entity';
import { TravelMood } from '../travelMoods/travelMood.entity';

describe('TravelService', () => {
  let service: TravelService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TravelService,
        MoodsService,
        {
          provide: getRepositoryToken(Tour),
          useValue: {
            find: jest.fn().mockResolvedValue([]),
          },
        },
        {
          provide: getRepositoryToken(TravelMood),
          useValue: {
            find: jest.fn().mockResolvedValue([]),
          },
        },
        {
          provide: getRepositoryToken(Travel),
          useValue: {
            find: jest.fn().mockResolvedValue([]),
          },
        },
        {
          provide: getRepositoryToken(Mood),
          useValue: {
            find: jest.fn().mockResolvedValue([]),
          },
        },
      ],
    }).compile();

    service = module.get<TravelService>(TravelService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
