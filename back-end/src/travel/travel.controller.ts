import {
  Body,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  Post,
  UseGuards,
} from '@nestjs/common';
import { RolesGuard } from '../roles/roles.guard';
import { RoleEnum, Roles } from '../roles/roles.decorator';
import { TravelService } from './travel.service';
import { Travel } from './travel.entity';
import { Paginate, Paginated, PaginateQuery } from 'nestjs-paginate';
import { TravelDto } from './travel.dto';
import { TravelResponse } from './travel.response';
import { DeleteResult } from 'typeorm';
import { ApiCreatedResponse, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { Tour } from '../tours/tour.entity';
import { ToursService } from '../tours/tours.service';

@ApiTags('Travels')
@Controller('travels')
export class TravelController {
  constructor(
    private travelService: TravelService,
    private toursService: ToursService,
  ) {}

  @Get()
  @UseGuards(RolesGuard)
  @Roles(RoleEnum.EDITOR, RoleEnum.ADMIN)
  async getAll(@Paginate() query: PaginateQuery): Promise<Paginated<Travel>> {
    return this.travelService.findAll(query);
  }

  @Get('public')
  async getAllPublic(
    @Paginate() query: PaginateQuery,
  ): Promise<Paginated<Travel>> {
    return this.travelService.findAllPublic(query);
  }

  @ApiOkResponse({
    type: TravelResponse,
  })
  @Get(':id')
  @UseGuards(RolesGuard)
  @Roles(RoleEnum.EDITOR, RoleEnum.ADMIN)
  async getById(@Param('id') id: string): Promise<TravelResponse> {
    const travel = await this.travelService.findById(id);

    if (!travel) {
      throw new NotFoundException();
    }

    return travel;
  }

  @Get(':travelSlug/tours')
  async getByTravelSlug(
    @Param('travelSlug') travelSlug: string,
    @Paginate() query: PaginateQuery,
  ): Promise<Paginated<Tour>> {
    return this.toursService.findByTravelSlug(query, travelSlug);
  }

  @ApiCreatedResponse({
    type: TravelResponse,
  })
  @Post()
  @UseGuards(RolesGuard)
  @Roles(RoleEnum.ADMIN)
  async create(@Body() newTravel: TravelDto): Promise<TravelResponse> {
    return this.travelService.create(newTravel);
  }

  @ApiOkResponse({
    type: DeleteResult,
  })
  @Delete(':id')
  @UseGuards(RolesGuard)
  @Roles(RoleEnum.ADMIN)
  async remove(@Param('id') id: string): Promise<DeleteResult> {
    return this.travelService.delete(id);
  }
}
