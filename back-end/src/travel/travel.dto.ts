import { TravelMoodDto } from '../travelMoods/travelMood.dto';
import {
  ArrayMinSize,
  IsArray,
  IsBoolean,
  IsNotEmpty,
  IsNumber,
  Min,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';

export class TravelDto {
  @IsNotEmpty()
  slug: string;

  @IsNotEmpty()
  name: string;

  @IsBoolean()
  isPublic: boolean;

  @IsNotEmpty()
  description: string;

  @IsNumber()
  @Min(0)
  numberOfDays: number;

  @IsArray()
  @ValidateNested({ each: true })
  @ArrayMinSize(1)
  @Type(() => TravelMoodDto)
  moods: TravelMoodDto[];
}
