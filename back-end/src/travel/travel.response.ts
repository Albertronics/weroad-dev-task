import { Tour } from '../tours/tour.entity';

export class TravelResponse {
  id: string;
  isPublic: boolean;
  slug: string;
  name: string;
  description: string;
  numberOfDays: number;
  numberOfNights: number;
  moods: { [key: string]: number };
  tours: Tour[];
}
