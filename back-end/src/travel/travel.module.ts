import { Module } from '@nestjs/common';
import { TravelController } from './travel.controller';
import { TravelService } from './travel.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Travel } from './travel.entity';
import { TravelMood } from '../travelMoods/travelMood.entity';
import { ToursModule } from '../tours/tours.module';

@Module({
  imports: [ToursModule, TypeOrmModule.forFeature([Travel, TravelMood])],
  controllers: [TravelController],
  providers: [TravelService],
})
export class TravelModule {}
