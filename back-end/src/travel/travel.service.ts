import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import {
  DeleteResult,
  getConnection,
  Repository,
  SelectQueryBuilder,
} from 'typeorm';
import { Travel } from './travel.entity';
import { TravelMood } from '../travelMoods/travelMood.entity';
import { PaginateQuery, paginate, Paginated } from 'nestjs-paginate';
import { TravelDto } from './travel.dto';
import { TravelResponse } from './travel.response';

@Injectable()
export class TravelService {
  constructor(
    @InjectRepository(Travel)
    private travelRepository: Repository<Travel>,
    @InjectRepository(TravelMood)
    private travelMoodRepository: Repository<TravelMood>,
  ) {}

  async create(newTravel: TravelDto): Promise<TravelResponse> {
    const connection = getConnection();
    const queryRunner = connection.createQueryRunner();

    try {
      await queryRunner.startTransaction();

      const travelInstance: Travel = this.travelRepository.create(newTravel);
      const savedTravel: Travel = await queryRunner.manager.save(
        travelInstance,
      );

      const moodsInstances: TravelMood[] = travelInstance.moods.map((el) =>
        this.travelMoodRepository.create({ travel: savedTravel, ...el }),
      );

      if (moodsInstances.length > 0) {
        await queryRunner.manager.save(moodsInstances);
      }

      await queryRunner.commitTransaction();

      return {
        ...savedTravel,
        moods: moodsInstances.reduce(
          (obj, item) => ({
            ...obj,
            [item.mood.name]: item.value,
          }),
          {},
        ),
      };
    } catch (error) {
      await queryRunner.rollbackTransaction();

      if (error?.code == 23505) {
        throw new BadRequestException(error?.detail);
      }

      throw new InternalServerErrorException();
    } finally {
      await queryRunner.release();
    }
  }

  findAll(query: PaginateQuery): Promise<Paginated<Travel>> {
    const qb: SelectQueryBuilder<Travel> = this.travelRepository
      .createQueryBuilder('travel')
      .innerJoinAndSelect('travel.moods', 'travelMood')
      .innerJoinAndSelect('travelMood.mood', 'mood');

    return paginate<Travel>(query, qb, {
      sortableColumns: ['name', 'numberOfDays'],
      searchableColumns: ['name', 'numberOfDays', 'isPublic'],
      defaultSortBy: [],
      filterableColumns: {},
    });
  }

  findAllPublic(query: PaginateQuery): Promise<Paginated<Travel>> {
    const qb: SelectQueryBuilder<Travel> = this.travelRepository
      .createQueryBuilder('travel')
      .innerJoinAndSelect('travel.moods', 'travelMood')
      .innerJoinAndSelect('travelMood.mood', 'mood')
      .leftJoinAndSelect('travel.tours', 'tour')
      .where({ isPublic: true });

    return paginate<Travel>(query, qb, {
      sortableColumns: ['name', 'numberOfDays'],
      searchableColumns: ['name', 'numberOfDays'],
      defaultSortBy: [],
      filterableColumns: {},
    });
  }

  delete(id: string): Promise<DeleteResult> {
    return this.travelRepository.delete(id);
  }

  async findById(id: string): Promise<TravelResponse | undefined> {
    const travel = await this.travelRepository
      .createQueryBuilder('travel')
      .innerJoinAndSelect('travel.moods', 'travelMood')
      .innerJoinAndSelect('travelMood.mood', 'mood')
      .leftJoinAndSelect('travel.tours', 'tour')
      .where('travel.id = :travelId', { travelId: id })
      .getOne();

    return travel as unknown as TravelResponse;
  }
}
