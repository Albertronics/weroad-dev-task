import { MigrationInterface, QueryRunner } from 'typeorm';

export class RoleData1646345227755 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    queryRunner.query(`
        INSERT INTO roles (id, name) VALUES ('b64f0542-4203-4e6a-b57d-2f4f7d868e16', 'admin'),
          ('85078f93-c186-4445-a98c-884a9c745927', 'editor');
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    queryRunner.clearTable('roles');
  }
}
