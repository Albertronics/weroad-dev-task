import { MigrationInterface, QueryRunner } from 'typeorm';

export class TravelMoodTable1646345520895 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    queryRunner.query(`
      create table travel_mood
      (
        value smallint not null,
        "travelId" uuid not null
          constraint "FK_7c9017df468d90524288e6291ce"
          references travels
          on delete cascade,
        "moodId" uuid not null
          constraint "FK_8a7ca083c65cf2d4d66ba9f8e00"
          references moods,
          constraint "PK_cadf1a3ea0611083825c75c1a26"
        primary key ("travelId", "moodId")
      );
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    queryRunner.dropTable('travel_mood');
  }
}
