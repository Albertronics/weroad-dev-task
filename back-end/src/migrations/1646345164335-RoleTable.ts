import { MigrationInterface, QueryRunner } from 'typeorm';

export class RoleTable1646345164335 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    queryRunner.query(`
        create table roles
        (
            id uuid default uuid_generate_v4() not null
                constraint "PK_b36bcfe02fc8de3c57a8b2391c2"
                primary key,
            name varchar not null
        );
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    queryRunner.dropTable('roles');
  }
}
