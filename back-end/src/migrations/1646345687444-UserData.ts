import { MigrationInterface, QueryRunner } from 'typeorm';

export class UserData1646345687444 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    queryRunner.query(`
        INSERT INTO "users" (id, email, password) VALUES 
          ('f68ec16e-0d05-4dd0-899f-4c10448ec3e6', 'albertomusciumarra@gmail.com', '$2b$12$RP6dMYdCorkHTvVVXCFMqOy53BfVpkRzyKqTsiGDErbRmmUjXu1Eq');
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    queryRunner.clearTable('users');
  }
}
