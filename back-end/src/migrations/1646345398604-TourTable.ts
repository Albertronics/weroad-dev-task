import { MigrationInterface, QueryRunner } from 'typeorm';

export class TourTable1646345398604 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    queryRunner.query(`
      create table tours
      (
        id uuid default uuid_generate_v4() not null
          constraint "PK_08169baf38d66dda14e7a321d9a"
          primary key,
        name varchar not null
          constraint "UQ_948c1044932dba70d131655953d"
          unique,
        "startingDate" date not null,
        "endingDate" date not null,
        price integer not null,
        "travelId" uuid not null
          constraint "FK_e507c3e1c94d3608080849fe444"
          references travels
          on delete cascade
      );
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    queryRunner.dropTable('tours');
  }
}
