import { MigrationInterface, QueryRunner } from 'typeorm';

export class UserRoleData1646345812085 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    queryRunner.query(`
        INSERT INTO user_roles_role ("userId", "roleId") VALUES 
          ('f68ec16e-0d05-4dd0-899f-4c10448ec3e6', 'b64f0542-4203-4e6a-b57d-2f4f7d868e16');
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    queryRunner.clearTable('user_roles_role');
  }
}
