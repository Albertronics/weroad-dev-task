import { MigrationInterface, QueryRunner } from 'typeorm';

export class MoodData1646345010643 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    queryRunner.query(`
        INSERT INTO moods (id, name) VALUES ('94204aea-4713-4c6f-9307-49d66340fb05', 'history'),
         ('28e7bba4-d280-40b1-833f-ca64c9af1dd9', 'nature'),
         ('410ba91a-34b7-4528-849b-eacb833fb79a', 'relax'),
         ('19eeb360-fe8c-478a-9f3d-1bc267a2f6f2', 'culture'),
         ('835ae233-8993-46ac-8c6d-76817051736a', 'party');
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    queryRunner.clearTable('moods');
  }
}
