import { MigrationInterface, QueryRunner } from 'typeorm';

export class TravelTable1646345302455 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    queryRunner.query(`
      create table travels
      (
        id uuid default uuid_generate_v4() not null
          constraint "PK_657b63ec7adcf2ecf757a490a67"
          primary key,
        "isPublic" boolean default false not null,
        slug varchar not null,
        name varchar not null
          constraint "UQ_05f9b8cc27aaff6e2cd51382a7f"
          unique,
        description text not null,
        "numberOfDays" smallint not null
      );
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    queryRunner.dropTable('travels');
  }
}
