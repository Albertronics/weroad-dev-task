import { MigrationInterface, QueryRunner } from 'typeorm';

export class UserRoleTable1646345747315 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    queryRunner.query(`
      create table user_roles_role
      (
        "userId" uuid not null
          constraint "FK_5f9286e6c25594c6b88c108db77"
          references "users"
          on update cascade on delete cascade,
        "roleId" uuid not null
          constraint "FK_4be2f7adf862634f5f803d246b8"
          references roles
          on update cascade on delete cascade,
          constraint "PK_b47cd6c84ee205ac5a713718292"
        primary key ("userId", "roleId")
      );
      
      create index "IDX_5f9286e6c25594c6b88c108db7"
      on user_roles_role ("userId");
    
      create index "IDX_4be2f7adf862634f5f803d246b"
      on user_roles_role ("roleId");
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    queryRunner.dropTable('user_roles_role');
  }
}
