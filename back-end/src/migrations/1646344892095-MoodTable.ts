import { MigrationInterface, QueryRunner } from 'typeorm';

export class MoodTable1646344892095 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    queryRunner.query(`
        create table moods
        (
            id uuid default uuid_generate_v4() not null
              constraint "PK_cd069bf46deedf0ef3a7771f44b"
              primary key,
            name varchar not null
        );
      `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    queryRunner.dropTable('moods');
  }
}
