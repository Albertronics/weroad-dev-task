import { MigrationInterface, QueryRunner } from 'typeorm';

export class UserTable1646345608335 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    queryRunner.query(`
      create table "users"
      (
        id uuid default uuid_generate_v4() not null
          constraint "PK_cace4a159ff9f2512dd42373760"
          primary key,
        email varchar not null
          constraint "UQ_e12875dfb3b1d92d7d7c5377e22"
          unique,
        password varchar not null
      );
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    queryRunner.dropTable('users');
  }
}
