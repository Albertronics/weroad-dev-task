import { Module } from '@nestjs/common';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RolesModule } from './roles/roles.module';
import { User } from './users/user.entity';
import { Role } from './roles/role.entity';
import { TravelModule } from './travel/travel.module';
import { Travel } from './travel/travel.entity';
import { TravelMood } from './travelMoods/travelMood.entity';
import { Mood } from './moods/mood.entity';
import { ToursModule } from './tours/tours.module';
import { Tour } from './tours/tour.entity';
import { ConfigModule } from '@nestjs/config';
import { MoodsModule } from './moods/moods.module';

@Module({
  imports: [
    AuthModule,
    UsersModule,
    ConfigModule.forRoot({
      envFilePath: '.dev.env',
      ignoreEnvFile: process.env.NODE_ENV == 'production',
    }),
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: process.env.DATABASE_HOST,
      port: parseInt(process.env.DATABASE_PORT),
      username: process.env.DATABASE_USER,
      password: process.env.DATABASE_PASSWROD,
      database: process.env.DATABASE_NAME,
      entities: [User, Role, Travel, TravelMood, Mood, Tour],
      migrationsRun: true,
      migrations: ['dist/migrations/*.js'],
      synchronize: false,
    }),
    RolesModule,
    TravelModule,
    ToursModule,
    MoodsModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
