import { Entity, Column, ManyToOne } from 'typeorm';
import { Travel } from '../travel/travel.entity';
import { Mood } from '../moods/mood.entity';

@Entity()
export class TravelMood {
  @Column({ type: 'smallint' })
  value: number;

  @ManyToOne(() => Travel, (travel) => travel.moods, {
    primary: true,
    onDelete: 'CASCADE',
  })
  travel: Travel;

  @ManyToOne(() => Mood, (mood) => mood.travels, { primary: true })
  mood: Mood;
}
