import {
  IsDefined,
  IsNotEmpty,
  IsNotEmptyObject,
  IsNumber,
  Min,
  ValidateNested,
} from 'class-validator';
import { MoodDto } from '../moods/mood.dto';
import {Type} from "class-transformer";

export class TravelMoodDto {
  @IsNotEmpty()
  @IsNumber()
  @Min(0)
  value: number;

  @IsDefined()
  @IsNotEmptyObject()
  @ValidateNested()
  @Type(() => MoodDto)
  mood: MoodDto;
}
