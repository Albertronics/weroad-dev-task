import { IsNotEmpty, IsNumber, Min } from 'class-validator';

export class TourDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  startingDate: Date;

  @IsNotEmpty()
  endingDate: Date;

  @IsNotEmpty()
  @IsNumber()
  @Min(1)
  price: number;

  @IsNotEmpty()
  travelId: string;
}
