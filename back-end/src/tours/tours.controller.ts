import {
  Body,
  Controller,
  Get,
  HttpCode,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { RolesGuard } from '../roles/roles.guard';
import { RoleEnum, Roles } from '../roles/roles.decorator';
import { ToursService } from './tours.service';
import { TourDto } from './tour.dto';
import { Paginate, Paginated, PaginateQuery } from 'nestjs-paginate';
import { Tour } from './tour.entity';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Tours')
@Controller('tours')
export class ToursController {
  constructor(private toursService: ToursService) {}

  @Get()
  @UseGuards(RolesGuard)
  @Roles(RoleEnum.EDITOR, RoleEnum.ADMIN)
  async getAll(@Paginate() query: PaginateQuery): Promise<Paginated<Tour>> {
    return this.toursService.findAll(query);
  }

  @Get(':id')
  @UseGuards(RolesGuard)
  @Roles(RoleEnum.EDITOR, RoleEnum.ADMIN)
  async getById(@Param('id') id: string): Promise<Tour> {
    return this.toursService.findById(id);
  }

  @Post()
  @UseGuards(RolesGuard)
  @Roles(RoleEnum.ADMIN)
  async create(@Body() newTour: TourDto): Promise<Tour> {
    return this.toursService.create(newTour);
  }

  @Put(':id')
  @UseGuards(RolesGuard)
  @Roles(RoleEnum.EDITOR, RoleEnum.ADMIN)
  @HttpCode(204)
  async edit(@Body() updatedTour: TourDto, @Param('id') id: string) {
    await this.toursService.modify(updatedTour, id);
  }
}
