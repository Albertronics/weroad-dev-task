import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { Travel } from '../travel/travel.entity';

@Entity('tours')
export class Tour {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ unique: true })
  name: string;

  @Column({ type: 'date' })
  startingDate: Date;

  @Column({ type: 'date' })
  endingDate: Date;

  @Column({ type: 'int' })
  price: number;

  @ManyToOne(() => Travel, (travel) => travel.moods, {
    onDelete: 'CASCADE',
  })
  travel: Travel;
}
