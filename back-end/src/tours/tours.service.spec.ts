import { Test, TestingModule } from '@nestjs/testing';
import { ToursService } from './tours.service';
import { MoodsService } from '../moods/moods.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Tour } from './tour.entity';
import { Mood } from '../moods/mood.entity';
import { Travel } from '../travel/travel.entity';
import { TourDto } from './tour.dto';

const relatedTravel: Travel = {
  numberOfNights: 0,
  tours: [],
  isPublic: true,
  slug: 'jordan-360',
  name: 'Jordan 360°as',
  description:
    'Jordan 360°: the perfect tour to discover the suggestive Wadi Rum desert, the ancient beauty of Petra, and much more.\n\nVisiting Jordan is one of the most fascinating things that everyone has to do once in their life.You are probably wondering "Why?". Well, that\'s easy: because this country keeps several passions! During our tour in Jordan, you can range from well-preserved archaeological masterpieces to trekkings, from natural wonders excursions to ancient historical sites, from camels trek in the desert to some time to relax.\nDo not forget to float in the Dead Sea and enjoy mineral-rich mud baths, it\'s one of the most peculiar attractions. It will be a tour like no other: this beautiful country leaves a memorable impression on everyone.',
  numberOfDays: 8,
  moods: [],
  id: 'de0dc4a8-a979-45ec-853a-ef019672bf6e',
} as Travel;

const tourDto: TourDto = {
  travelId: '79673bb0-9978-4a3e-a80d-5c0cb837c068',
  name: 'ITJOR20211101s',
  startingDate: new Date(Date.parse('2021-11-01')),
  endingDate: new Date(Date.parse('2021-11-09')),
  price: 199900,
};

const tourInstance: Tour = {
  id: null,
  travel: relatedTravel,
  name: 'ITJOR20211101s',
  startingDate: new Date(Date.parse('2021-11-01')),
  endingDate: new Date(Date.parse('2021-11-09')),
  price: 199900,
};

const createdTour = {
  name: 'ITJOR20211101s',
  startingDate: new Date(Date.parse('2021-11-01')),
  endingDate: new Date(Date.parse('2021-11-09')),
  price: 199900,
  travel: {
    id: '79673bb0-9978-4a3e-a80d-5c0cb837c068',
    isPublic: true,
    slug: 'jordan-360',
    name: 'Jordan 360°as',
    description:
      'Jordan 360°: the perfect tour to discover the suggestive Wadi Rum desert, the ancient beauty of Petra, and much more.\n\nVisiting Jordan is one of the most fascinating things that everyone has to do once in their life.You are probably wondering "Why?". Well, that\'s easy: because this country keeps several passions! During our tour in Jordan, you can range from well-preserved archaeological masterpieces to trekkings, from natural wonders excursions to ancient historical sites, from camels trek in the desert to some time to relax.\nDo not forget to float in the Dead Sea and enjoy mineral-rich mud baths, it\'s one of the most peculiar attractions. It will be a tour like no other: this beautiful country leaves a memorable impression on everyone.',
    numberOfDays: 8,
    numberOfNights: 7,
  },
  id: '1da2493f-46ce-4676-8ee5-1b406955a878',
};

describe('ToursService', () => {
  let service: ToursService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ToursService,
        MoodsService,
        {
          provide: getRepositoryToken(Tour),
          useValue: {
            save: jest.fn().mockResolvedValue(createdTour),
            create: jest.fn().mockResolvedValue(tourInstance),
          },
        },
        {
          provide: getRepositoryToken(Travel),
          useValue: {
            findOne: jest.fn().mockResolvedValue(relatedTravel),
          },
        },
        {
          provide: getRepositoryToken(Mood),
          useValue: {
            find: jest.fn().mockResolvedValue([]),
          },
        },
      ],
    }).compile();

    service = module.get<ToursService>(ToursService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('create new tour', async () => {
    const result = await service.create(tourDto);

    expect(result.id).toBeDefined();
  });
});
