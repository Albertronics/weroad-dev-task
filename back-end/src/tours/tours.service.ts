import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, SelectQueryBuilder, UpdateResult } from 'typeorm';
import { Tour } from './tour.entity';
import { TourDto } from './tour.dto';
import { Travel } from '../travel/travel.entity';
import {
  FilterOperator,
  paginate,
  Paginated,
  PaginateQuery,
} from 'nestjs-paginate';

@Injectable()
export class ToursService {
  constructor(
    @InjectRepository(Tour)
    private tourRepository: Repository<Tour>,
    @InjectRepository(Travel)
    private travelRepository: Repository<Travel>,
  ) {}

  private async checkTravelExists(tour: TourDto): Promise<Travel> {
    const relatedTravel: Travel = await this.travelRepository.findOne(
      tour.travelId,
    );

    if (!relatedTravel) {
      throw new BadRequestException('Invalid travelId');
    }

    return relatedTravel;
  }

  async create(newTour: TourDto): Promise<Tour> {
    try {
      const relatedTravel: Travel = await this.checkTravelExists(newTour);

      const tourInstance: Tour = this.tourRepository.create(newTour);
      tourInstance.travel = relatedTravel;

      return await this.tourRepository.save(tourInstance);
    } catch (error: BadRequestException | any) {
      if (error.status === 400) {
        throw error;
      }

      if (error?.code == 23505) {
        throw new BadRequestException(error?.detail);
      }

      throw new InternalServerErrorException();
    }
  }

  async modify(updatedTour: TourDto, id: string): Promise<UpdateResult> {
    const tour: Tour = await this.tourRepository.findOne(id);

    if (!tour) {
      throw new NotFoundException(`Tour with ID ${id} not found`);
    }

    const relatedTravel: Travel = await this.checkTravelExists(updatedTour);

    const updatedTourInstance: Tour = this.tourRepository.create(updatedTour);
    updatedTourInstance.travel = relatedTravel;

    return this.tourRepository.update(id, updatedTourInstance);
  }

  findByTravelSlug(
    query: PaginateQuery,
    travelSlug: string,
  ): Promise<Paginated<Tour>> {
    const qb = this.tourRepository
      .createQueryBuilder('tour')
      .innerJoinAndSelect('tour.travel', 'travel')
      .where('travel.slug = :travelSlug', { travelSlug });

    if (!query.sortBy) {
      query.sortBy = [];
    }

    query.sortBy = [...query.sortBy, ['startingDate', 'ASC']];

    return paginate<Tour>(query, qb, {
      sortableColumns: ['price', 'startingDate'],
      defaultSortBy: [],
      filterableColumns: {
        price: [
          FilterOperator.GT,
          FilterOperator.LT,
          FilterOperator.GTE,
          FilterOperator.LTE,
          FilterOperator.BTW,
        ],
        startingDate: [FilterOperator.EQ],
        endingDate: [FilterOperator.EQ],
      },
    });
  }

  findAll(query: PaginateQuery): Promise<Paginated<Tour>> {
    const qb: SelectQueryBuilder<Tour> = this.tourRepository
      .createQueryBuilder('tour')
      .innerJoinAndSelect('tour.travel', 'travel');

    return paginate<Tour>(query, qb, {
      sortableColumns: ['name', 'price'],
    });
  }

  findById(id: string): Promise<Tour> {
    return this.tourRepository
      .createQueryBuilder('tour')
      .innerJoinAndSelect('tour.travel', 'travel')
      .innerJoinAndSelect('travel.moods', 'travelMood')
      .innerJoinAndSelect('travelMood.mood', 'mood')
      .where({ id })
      .getOneOrFail();
  }
}
