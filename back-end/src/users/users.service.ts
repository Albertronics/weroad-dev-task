import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import { User } from './user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserDto } from './user.dto';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}

  async findByEmail(email: string): Promise<User | undefined> {
    const [user] = await this.usersRepository.find({
      relations: ['roles'],
      where: { email },
    });

    return user;
  }

  async create(newUser: UserDto) {
    try {
      const userInstance: User = this.usersRepository.create(newUser);
      const savedUser: User = await this.usersRepository.save(userInstance);

      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { password, ...rest } = savedUser;
      return rest;
    } catch (error) {
      if (error?.code == 23505) {
        throw new BadRequestException(error?.detail);
      }

      throw new InternalServerErrorException();
    }
  }
}
