import { Test, TestingModule } from '@nestjs/testing';
import { UsersService } from './users.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { User } from './user.entity';
import { randomUUID } from 'crypto';

const userDto = {
  email: 'albertomusciumarra@gmail.com',
  password: '1234',
  roles: [{ id: 'b64f0542-4203-4e6a-b57d-2f4f7d868e16', name: 'admin' }],
};

const user = {
  id: randomUUID(),
  email: 'albertomusciumarra@gmail.com',
  password: '$2b$12$RP6dMYdCorkHTvVVXCFMqOy53BfVpkRzyKqTsiGDErbRmmUjXu1Eq',
  roles: [{ id: 'b64f0542-4203-4e6a-b57d-2f4f7d868e16', name: 'admin' }],
};

describe('UsersService', () => {
  let service: UsersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UsersService,
        {
          provide: getRepositoryToken(User),
          useValue: {
            create: jest.fn().mockResolvedValue(userDto as User),
            save: jest.fn().mockResolvedValue(user),
          },
        },
      ],
    }).compile();

    service = module.get<UsersService>(UsersService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('create new user', async () => {
    const result = await service.create(userDto);

    expect(result).not.toContain('password');
    expect(result.id).toBeDefined();
  });
});
