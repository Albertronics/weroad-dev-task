# Full stack Javascript/Typescript hiring test

### How to run
In order to run the whole project only docker-compose is needed. All the needed
services are declared inside `docker-compose.yaml` file in the correct start up order;
the database service is linked to a dedicated volume to persist its data, meanwhile
the API service will create and initialize all the tables.

##### TL:DR

`cd weroad-dev-task`\
`docker-compose up`

### Testing
Automated testing has been implemented for the back-end API and can be executed
by running:

`npm run test`

or with coverage percentages:

`npm run test:cov`

### Swagger

In order to facilitate integration and use of the APIs a Swagger page documenting
all the endpoints has been made available at: http://localhost:3000/api
