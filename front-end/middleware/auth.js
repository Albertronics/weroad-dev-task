export default ({ redirect, store }) => {
  if (!store.getters['auth/getUser']) {
    return redirect('/login')
  }
}
