const cookieparser = process.server ? require('cookieparser') : undefined

export const actions =
{
  async nuxtServerInit ({ commit }, { req, redirect }) {
    try {
      if (req.headers.cookie) {
        const { accessToken } = cookieparser.parse(req.headers.cookie)

        if (accessToken) {
          const user = await this.$axios.$get('auth/profile')
          commit('auth/setUser', user)
        } else {
          commit('auth/setUser', null)
          redirect('/login')
        }
      }
    } catch (e) {
      commit('setUser', null)
      redirect('/login')
    }
  }
}
