import Vue from 'vue';

Vue.filter('truncate', function (text, length, clamp) {
  text = text || ''
  clamp = clamp || '...'
  length = length || 30

  if (text.length <= length) { return text }

  let tcText = text.slice(0, length - clamp.length)
  let last = tcText.length - 1

  while (last > 0 && tcText[last] !== ' ' && tcText[last] !== clamp[0]) { last -= 1 }

  // Fix for case when text dont have any `space`
  last = last || length - clamp.length

  tcText = tcText.slice(0, last)

  return tcText + clamp
})

Vue.filter('formatCurrency', (val, addCurrencySymbol = true) => {
  val = parseInt(val) / 100
  return val.toFixed(2).replace('.', ',').replace(/\B(?=(\d{3})+(?!\d))/g, '.') + (addCurrencySymbol ? ' € ' : '')
})

Vue.filter('formatDate', (val, locale = 'en', format = 'MM/DD/YYYY') => {
  const d = new Date(val)

  if (!(d instanceof Date && !isNaN(d))) {
    return 'Data invalida'
  }

  const ye = new Intl.DateTimeFormat(locale, { year: 'numeric' }).format(d)
  const mo = new Intl.DateTimeFormat(locale, { month: '2-digit' }).format(d)
  const da = new Intl.DateTimeFormat(locale, { day: '2-digit' }).format(d)

  return format.toString()
    .replace(/MM/, mo)
    .replace(/DD/, da)
    .replace(/YYYY/, ye)
})
