export default (context, inject) => {
  const rules = {
    basicRules: [
      v => !!v || 'Campo richiesto'
    ],

    basicArrayRules: [
      v => (!!v && v.length > 0) || 'Campo richiesto'
    ],

    percentageRules: [
      v => !!v || 'Campo richiesto',
      v => (v >= 1 && v <= 100) || 'Valore fra 1 e 100'
    ]
  }

  inject('rules', rules)
}
